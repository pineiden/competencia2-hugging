from collections import Counter, OrderedDict

import torch

from torch.utils.data import DataLoader, Dataset
from torch.nn.utils.rnn import pad_sequence
from torchtext.vocab import vocab


class TaggingDataset(Dataset):
    def __init__(self, path, lower=False, separator=" ", encoding="utf-8"):

        with open(path, 'r', encoding=encoding) as file:
            text, tag, data = [], [], []
            for line in file:
                line = line.strip()
                if line == "":
                    data.append(dict({'text': text, 'nertags': tag}))
                    text, tag = [], []
                else:
                    line_content = line.split(separator)  # .rstrip('\n')
                    if lower:
                        text.append(line_content[0].lower())
                    else:
                        text.append(line_content[0])
                    tag.append(line_content[1])
        data.append(dict({'text': text, 'nertags': tag}))

        self.data = data

    def __len__(self):
        return len(self.data)

    def __getitem__(self, index):
        item = self.data[index]
        text = item["text"]
        nertags = item["nertags"]
        return nertags, text

    @property
    def labels(self):
        tags = set([tag for tag, _ in self])
        return sorted(tags)


def fit_vocab(data_iter):

    def update_counter(counter_obj):
        sorted_by_freq_tuples = sorted(counter_obj.items(),
                                       key=lambda x: x[1],
                                       reverse=True)
        ordered_dict = OrderedDict(sorted_by_freq_tuples)
        return ordered_dict

    counter_1 = Counter()
    counter_2 = Counter()
    for _nertags, _text in data_iter:
        counter_1.update(_text)
        counter_2.update(_nertags)

    od1 = update_counter(counter_1)
    od2 = update_counter(counter_2)

    v1 = vocab(od1, specials=['<PAD>', '<unk>'])
    v1.set_default_index(v1["<unk>"])
    v2 = vocab(od2, specials=['<PAD>'])

    def text_pipeline(x): return v1(x)
    def nertags_pipeline(x): return v2(x)

    return text_pipeline, nertags_pipeline, v1, v2


def collate_batch(batch):
    nertags_list, text_list = [], []
    for _nertags, _text in batch:
        processed_nertags = torch.tensor(nertags_pipeline(_nertags),
                                         dtype=torch.int64)
        nertags_list.append(processed_nertags)
        processed_text = torch.tensor(text_pipeline(_text),
                                      dtype=torch.int64)
        text_list.append(processed_text)
    nertags_list = pad_sequence(nertags_list, batch_first=True).T
    text_list = pad_sequence(text_list, batch_first=True).T
    return nertags_list.to(device), text_list.to(device)
