from tagging_dataset import TaggingDataset, fit_vocab
from pathlib import Path


def read_dataset():
    base = Path(__file__).parent
    train_iter = TaggingDataset(base / "train.txt")
    dev_iter = TaggingDataset(base / "dev.txt")
    test_iter = TaggingDataset(base / "test.txt")
    a, b, c, NER_TAGS = fit_vocab(train_iter)
    labels = NER_TAGS.vocab.get_itos()
    return {
        "train": train_iter,
        "test": test_iter,
        "dev": dev_iter
    }, labels
