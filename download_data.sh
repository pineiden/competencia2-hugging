# /bin/bash
wget https://github.com/dccuchile/CC6205/releases/download/v1.0/train.txt -nc # Dataset de Entrenamiento
wget https://github.com/dccuchile/CC6205/releases/download/v1.0/dev.txt -nc    # Dataset de Validación (Para probar y ajustar el modelo)
wget https://github.com/dccuchile/CC6205/releases/download/v1.0/test.txt -nc 
